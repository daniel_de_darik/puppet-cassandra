class nginx {

    package { 'nginx':
        ensure => installed,
        require => Exec['apt-get-update'],
    }

    service { 'nginx':
        enable      => true,
        ensure      => running,
    }
}
