class cassandra::setup {
    package { 'cassandra':
        ensure => installed,
        install_options => ['-y', '-q'],
        require => [
            Exec['apt-get-update'],
            Class['java'],
        ]
    }
}
