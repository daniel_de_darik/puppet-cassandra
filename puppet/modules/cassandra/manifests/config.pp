class cassandra::config {
    $ip = lookup('ip')
    $seed_providers = lookup('seed_providers')
    $topology = lookup('topology')

    file { '/etc/cassandra/cassandra.yaml':
        ensure => present,
        content => template('cassandra/cassandra.yaml.erb'),
        notify => Service['cassandra'],
    }

    file { '/etc/cassandra/cassandra-rackdc.properties':
        ensure => present,
        content => template('cassandra/cassandra-rackdc.properties.erb'),
        notify => Service['cassandra'],
    }
}
