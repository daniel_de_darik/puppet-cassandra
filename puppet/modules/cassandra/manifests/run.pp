class cassandra::run {
    service { 'cassandra':
        enable => true,
        ensure => running,
    }
}
