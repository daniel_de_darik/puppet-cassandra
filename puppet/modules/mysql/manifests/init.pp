# Class: mysql
#
#
class mysql { 

    package { 'mysql-server':
        ensure => installed,
        require => Exec['apt-get-update']
    }

    service { 'mysql':
        enable      => true,
        ensure      => running,
        require => Package['mysql-server']
    }
}
