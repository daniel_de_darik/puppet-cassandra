# Class: system_base
# installs basic software tools and ppa keys
# puppet resource --types - list of resources
# puppet describe resource -sm (puppet describe package -sm)
# hiera -c hiera.yaml <var> ::environment=test - output <var> value accordig to config
class system_base {

    $apt_keys = ['cassandra-3.gpg.key']
    system_base::aptkey { $apt_keys:
    }

    file { '/etc/apt/sources.list.d/cassandra-3.sources.list':
        ensure => present,
        owner => 'root',
        group => 'root',
        mode => '0664',
        source => 'puppet:///modules/cassandra/cassandra3.sources.list',
    }

    exec { 'apt-get-update':
        command      => 'apt-get update',
        path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
        require => Exec['apt-key-adv'],
    }

    $basic_packages = ['vim', 'dstat', 'sysstat', 'iftop', 'tree', 'htop']
    package { $basic_packages:
        ensure => installed,
        require => Exec['apt-get-update'],
    }

    $purge_packages = []
    package { $purge_packages:
        ensure => purged,
    }

    exec { 'apt-key-adv':
        command      => 'apt-key adv --keyserver pool.sks-keyservers.net --recv-key A278B781FE4B2BDA',
        path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
        user => 'root',
        group => 'root',
        require => [
            File['/etc/apt/sources.list.d/cassandra-3.sources.list'],
            System_base::Aptkey['cassandra-3.gpg.key'],
        ],
    }

    define aptkey($key_name = $title) {
        file { "/tmp/${key_name}":
            ensure => present,
            owner => 'root',
            group => 'root',
            mode => '0644',
            source => "puppet:///modules/cassandra/${key_name}",
        }

        exec { "Import ${key_name} into apt store":
            user => 'root',
            group => 'root',
            path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
            command => "apt-key add /tmp/${key_name}",
            subscribe => File["/tmp/${key_name}"],
        }
    }

     # $ppa_key = ['ppa:webupd8team/java']
    # system_base::ppakey { $ppa_key:
    # }

    # Define: method for adding ppa key
    # Parameters:
    # key_name - ppa keyname
    #
    # define ppakey ($key_name = $title) {
    #     exec { "Import ${key_name} to apt keystore":
    #         command      => "add-apt-repository ${key_name}",
    #         environment => 'HOME=/root',
    #         path        => '/usr/bin:/bin',
    #         user        => 'root',
    #         group       => 'root',
    #         refreshonly => true,
    #         notify => Exec['apt-get-update']
    #     }
    # }
}
