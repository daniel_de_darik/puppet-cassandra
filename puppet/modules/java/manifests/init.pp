class java {
    
    package { 'default-jdk':
        ensure => installed,
        install_options => ['-y', '-q'],
    }
}
