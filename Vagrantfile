# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"

  config.vm.provider :virtualbox do |vb|
    # set the memory of virtual machine to 4GB
    # vb.memory = 4096
    # vb.cpus = 2
    vb.customize ["modifyvm", :id, "--memory", "3000"]
  end

  # disable default ssh port (configured for each port below)
  config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true

  config.vm.provision "bootstrap-puppet", type: "shell" do |shell|
    shell.path = "setup.sh"
  end

  config.vm.provision :puppet do |puppet|
    # environment is supported from version >= 4.0
    puppet.environment_path = "puppet/environments"
    puppet.environment = "test"
    puppet.hiera_config_path = "puppet/hiera.yaml"
    puppet.options = "--verbose --debug --trace"
  end

  nodes = [
    {'id' => 1, 'autostart' => true},
    # {'id' => 2, 'autostart' => true},
    {'id' => 3, 'autostart' => true},
  ]

  nodes.each do |node_config|
    name = "node-#{node_config['id']}"
    ip = "192.168.56.1#{node_config['id']}"
    ssh_port = "220#{node_config['id']}"
    
    config.vm.define name, autostart: node_config['autostart'] do |node|
      node.vm.hostname = "#{name}.local" #fqdn in puppet world
      # assign static ip address for the guest os
      node.vm.network "private_network", ip: ip
      # auto_correct - if set to true will dynamically adjust port if it collides with host
      # port 22 on guest os will be accessible via 2201 on from host machine
      node.vm.network :forwarded_port, guest: 22, host: ssh_port, auto_correct: false
      #  The port on the guest that SSH is running on. This is used by some providers to detect forwarded ports for SSH. For example, if this is set to 22 (the default), and Vagrant detects a forwarded port to port 22 on the guest from port 4567 on the host, Vagrant will attempt to use port 4567 to talk to the guest if there is no other option.
      node.ssh.guest_port = ssh_port
    end
  end
end
