#!/bin/bash
wget https://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
dpkg -i puppetlabs-release-pc1-trusty.deb
apt-get update -qq
apt-get install -y puppetserver

export PATH=$PATH:/opt/puppetlabs/bin
puppet resource service puppetserver ensure=running enable=true
